import { Icon } from '@iconify/react';
// import { sentenceCase } from 'change-case';
import plusFill from '@iconify/icons-eva/plus-fill';
import { Link as RouterLink } from 'react-router-dom';
// material
import {
  
  Stack,
  Button,
  Typography,
  Container,
  Grid,
  // Table,
  // Avatar,
  // Checkbox,
  // TableRow,
  // TableBody,
  // TableCell,
  // TableContainer,
  // TablePagination
} from '@material-ui/core';
// components
import Page from '../components/Page';
// import Label from '../components/Label';

import { UserCard } from '../components/_dashboard/user';
// import { orderBy } from 'lodash';

import USERS from '../_mocks_/user';



export default function User() {

  return (
    <Page title="User">
      <Container>
        <Stack direction="row" alignItems="center" justifyContent="space-between" mb={5}>
          <Typography variant="h4" gutterBottom>
            User
          </Typography>
          <Button
            variant="contained"
            component={RouterLink}
            to="#"
            startIcon={<Icon icon={plusFill} />}
          >
            New User
          </Button>
        </Stack>
{/* 
        <Card>
          <Scrollbar>
            Card
          </Scrollbar>
        </Card> */}

        <Grid container spacing={3}>
          {USERS.map((user, index) => (
            <UserCard key={user.id} user={user} index={index} />
          ))}
        </Grid>

      </Container>
    </Page>
    
  );
}



