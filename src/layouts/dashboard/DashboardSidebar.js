import PropTypes from 'prop-types';
import React, { useEffect } from 'react';
import { Link as RouterLink, useLocation } from 'react-router-dom';
// material
import { styled } from '@material-ui/core/styles';
import {
  Box,
  Drawer,
  Radio,
} from '@material-ui/core';
import { withStyles, makeStyles } from '@material-ui/styles';
import { green } from '@material-ui/core/colors';
// components
import Logo from '../../components/Logo';
import Scrollbar from '../../components/Scrollbar';
import NavSection from '../../components/NavSection';
import { MHidden } from '../../components/@material-extend';
//
import sidebarConfig from './SidebarConfig';

const DRAWER_WIDTH = 280;
const SIDEBAR_MINWIDTH = 80;

const RootStyle = styled('div')(({ theme, isfixedsidebar }) => ({
  [theme.breakpoints.up('lg')]: {
    flexShrink: 0,
    width: isfixedsidebar,
  }
}));

const GreenRadio = withStyles({
  root: {
    color: green[400],
    '&$checked': {
      color: green[600],
    },
  },
  checked: {},
})((props) => <Radio color="default" {...props} />);

DashboardSidebar.propTypes = {
  isOpenSidebar: PropTypes.bool,
  isFixedSidebar: PropTypes.bool,
  onCloseSidebar: PropTypes.func,
  onFixedSidebar: PropTypes.func,
  onSidebarHover: PropTypes.func,
};

const useStyles = makeStyles((theme) => ({
  menuButton: {
    marginRight: 36,
  },
  flex: {
    display: 'flex',
    justifyContent: 'space-between',
    paddingRight: 7,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: DRAWER_WIDTH,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: DRAWER_WIDTH,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(10),
    },
    '& .radio-fixed-sidebar': {
      display: 'none',
    },
    '&:hover': {
      width: DRAWER_WIDTH,
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
      '& .radio-fixed-sidebar': {
        display: 'block',
      },
    }
  },
}));

export default function DashboardSidebar({
  isOpenSidebar,
  isFixedSidebar,
  onCloseSidebar,
  onFixedSidebar,
  onSidebarHover,
}) {
  const classes = useStyles();
  const { pathname } = useLocation();
  const [open, setOpen] = React.useState(true);
  const [hover, setHover] = React.useState(false);

  const handleToggleSidebar = (event) => {
    setOpen(!open);
    onFixedSidebar(!open);
  }
  const handleHoverEvent = (event) => {
    setHover(event);
    onSidebarHover(event);
  }

  useEffect(() => {
    if (isOpenSidebar) {
      onCloseSidebar();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pathname]);

  const renderContentMobile = (
    <Scrollbar
      sx={{
        height: '100%',
        '& .simplebar-content': { height: '100%', display: 'flex', flexDirection: 'column' }
      }}
    >
      <Box sx={{ px: 2.5, py: 3 }}>
        <Box component={RouterLink} to="/" sx={{ display: 'inline-flex' }}>
          <Logo />
        </Box>
      </Box>

      <NavSection navConfig={sidebarConfig} open={true} hover={true}/>
    </Scrollbar>
  );

  const renderContent = (
    <Scrollbar
      sx={{
        height: '100%',
        '& .simplebar-content': { height: '100%', display: 'flex', flexDirection: 'column' }
      }}
    >
      <Box sx={{ px: 2.5, py: 3 }} className={classes.flex}>
        <Box component={RouterLink} to="/" sx={{ display: 'inline-flex' }}>
          <Logo />
        </Box>
        <GreenRadio
          checked={open}
          onClick={handleToggleSidebar}
          name="radio-fixed-sidebar"
          className={'radio-fixed-sidebar'}
        />
      </Box>

      <NavSection navConfig={sidebarConfig} open={open} hover={hover}/>
    </Scrollbar>
  );

  return (
    <RootStyle isfixedsidebar={isFixedSidebar ? DRAWER_WIDTH : SIDEBAR_MINWIDTH}>
      <MHidden width="lgUp">
        <Drawer
          open={isOpenSidebar}
          onClose={onCloseSidebar}
          PaperProps={{
            sx: { width: DRAWER_WIDTH }
          }}
          className={`${classes.drawer} ${classes.drawerOpen}`}
        >
          {renderContentMobile}
        </Drawer>
      </MHidden>

      <MHidden width="lgDown">
        <Drawer
          onMouseEnter={() => handleHoverEvent(true)}
          onMouseLeave={() => handleHoverEvent(false)}
          variant="permanent" 
          classes={{
            paper: open ? classes.drawerOpen : classes.drawerClose
          }}
          className={`${classes.drawer} ${open ? classes.drawerOpen : classes.drawerClose}`}
        >
          {renderContent}
        </Drawer>
      </MHidden>
    </RootStyle>
  );
}
