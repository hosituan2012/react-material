import React from 'react';
import PropTypes from 'prop-types';

import { makeStyles } from '@material-ui/styles';
import {
  alpha,
  Box,
  Drawer,
  Button,
  Typography,
  Grid,
} from '@material-ui/core';

import { Icon } from '@iconify/react';
import options2Fill from '@iconify/icons-eva/options-2-fill';
import sunFill from '@iconify/icons-eva/sun-fill';
import moonFill from '@iconify/icons-eva/moon-fill';

const useStyles = makeStyles({
  list: {
    width: 280,
    height: '100%',
    backgroundColor: theme => theme.palette.background.paper,
    color: theme => theme.palette.grey[800],
  },
  fullList: {
    width: 'auto',
  },
  button: {
    zIndex: 999,
    minWidth: 0,
    right: 0,
    display: 'flex',
    cursor: 'pointer',
    position: 'fixed',
    alignItems: 'center',
    top: '50%',
    height: theme => theme.spacing(5),
    marginTop: theme => `-${theme.spacing(5)}`,
    paddingLeft: theme => theme.spacing(1),
    paddingRight: theme => theme.spacing(1),
    boxShadow: theme => theme.customShadows.z20,
    color: theme => theme.palette.text.primary,
    backgroundColor: theme => theme.palette.background.paper,
    borderTopLeftRadius: theme => theme.shape.borderRadiusMd,
    borderBottomLeftRadius: theme => theme.shape.borderRadiusMd,
    borderTopRightRadius: 0,
    borderBottomRightRadius: 0,
    transition: theme => theme.transitions.create('opacity'),
  },
  btnBlock: {
    borderRadius: 8,
    '&:hover': {
      backgroundColor: theme => alpha(theme.palette.primary.main, 0.24),
    }
  },
  modeBtn: {
    width: '100%',
    height: 108,
    borderRadius: 8,
    backgroundColor: '#fff',
    color: theme => theme.palette.grey[600],
    '&:hover': {
      backgroundColor: alpha('#fff', 0.8),
    }
  },
  darkBtn: {
    backgroundColor: 'rgb(22, 28, 36) !important',
    color: 'rgb(145, 158, 171)',
    '&:hover': {
      backgroundColor: 'rgba(22, 28, 36, 0.8) !important',
    },
  },
  activeBtn: {
    color: theme => theme.palette.primary.main,
    boxShadow: theme => theme.customShadows.z20,
  },
});

SidebarSetting.propTypes = {
  theme: PropTypes.object,
  themeMode: PropTypes.string,
  onHandleChangeTheme: PropTypes.func,
};

export default function SidebarSetting({
  theme,
  themeMode,
  onHandleChangeTheme,
}) {
  const classes = useStyles(theme);
  const [open, setOpen] = React.useState(false);

  const toggleDrawer = (open) => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }
    setOpen(open);
  };

  const onChangeTheme = (value) => {
    localStorage.setItem('theme', value);
    onHandleChangeTheme(value);
  }

  return (
    <div>
      <React.Fragment>
        <Button onClick={toggleDrawer(true)} className={classes.button}>
          <Icon icon={options2Fill} width={24} height={24}/>
        </Button>
        <Drawer
          anchor={'right'}
          open={open}
          onClose={toggleDrawer(false)}
        >
          <div
            className={classes.list}
            role="presentation"
            onClick={toggleDrawer(false)}
            onKeyDown={toggleDrawer(false)}
          >
            <Button onClick={toggleDrawer(false)} className={classes.button} style={{ right: 280 }}>
              <Icon icon={options2Fill} width={24} height={24}/>
            </Button>
            <Box sx={{ px: 2.5, py: 3 }} className={classes.flex}>
              <Typography variant="h4" gutterBottom>
                Setting
              </Typography>
            </Box>
            <Box sx={{ px: 2.5, py: 3 }} className={classes.flex}>
              <Typography variant="subtitle1" gutterBottom>
                Mode
              </Typography>
              <Grid container spacing={3} >
                <Grid
                  key="light-mode-btn"
                  item
                  xs={12}
                  sm={6}
                  md={6}
                >
                  <div className={classes.btnBlock}>
                    <Button
                      className={`${classes.modeBtn} ${themeMode === 'light' ? classes.activeBtn : ''}`}
                      onClick={() => onChangeTheme('light')}
                    >
                      <Icon icon={sunFill} width={24} height={24}/>
                    </Button>
                  </div>
                </Grid>
                <Grid
                  key="dark-mode-btn"
                  item
                  xs={12}
                  sm={6}
                  md={6}
                >
                  <div className={classes.btnBlock}>
                    <Button
                      className={`${classes.modeBtn} ${classes.darkBtn} ${themeMode === 'light' ? '' : classes.activeBtn}`}
                      onClick={() => onChangeTheme('dark')}
                    >
                      <Icon icon={moonFill} width={24} height={24}/>
                    </Button>
                  </div>
                </Grid>
              </Grid>
            </Box>
          </div>
        </Drawer>
      </React.Fragment>
    </div>
  );
}
