import faker from 'faker';
import { sample } from 'lodash';
// utils
import { mockImgAvatar, mockImgCover } from '../utils/mockImages';

const users = [...Array(24)].map((_, index) => ({
  id: faker.datatype.uuid(),
  avatarUrl: mockImgAvatar(index + 1),
  coverUrl: mockImgCover(index + 1),
  name: faker.name.findName(),
  company: faker.company.companyName(),
  isVerified: faker.datatype.boolean(),
  status: sample(['active', 'banned']),
  role: sample([
    'Leader',
    'Hr Manager',
    'UI Designer',
    'UX Designer',
    'UI/UX Designer',
    'Project Manager',
    'Backend Developer',
    'Full Stack Designer',
    'Front End Developer',
    'Full Stack Developer'
  ]),
  followerFacebook: faker.datatype.number({min: 100, max: 99999}),
  followerInstagram: faker.datatype.number({min: 100, max: 99999}),
  followerTwitter: faker.datatype.number({min: 100, max: 99999}),
}));

export default users;
