import PropTypes from 'prop-types';
import { Icon } from '@iconify/react';
import { Link as RouterLink } from 'react-router-dom';

import instagram from '@iconify/icons-ant-design/instagram';
import linkedinFill from '@iconify/icons-ant-design/linkedin-fill';
import facebookFill from '@iconify/icons-eva/facebook-fill';
import TwitterFill from '@iconify/icons-eva/twitter-fill';

// material
import { styled } from '@material-ui/core/styles';
import { Box, Card, Grid, Avatar, Typography, CardContent, Divider } from '@material-ui/core';
// utils

import { kFormatter } from '../../../utils/formatNumber';
//
import SvgIconStyle from '../../SvgIconStyle';
// import { CenterFocusStrong, MicNone } from '@material-ui/icons';

const CardMediaStyle = styled('div')({
  position: 'relative',
  paddingTop: 'calc(100% * 3 / 4)'
});

const TitleStyle = styled('div')(({ theme }) => ({
  height: 36,
  fontSize: 16,
  textAlign: 'center',
  fontWeight: 600,
  WebkitLineClamp: 2,
  display: '-webkit-box',
  WebkitBoxOrient: 'vertical',
  marginTop: theme.spacing(2)

}));

const DividerStyle = styled(Divider)(({ theme }) => ({
  marginTop: theme.spacing(2)
}));

const FilterStyle = styled('div')({
  top: 0,
  width: '100%',
  height: '100%',
  objectFit: 'cover',
  position: 'absolute',
  backdropFilter: 'blur(3px)',
  background: 'rgba(0, 82, 73, 0.72)',
  opacity: 1
});

const AvatarStyle = styled(Avatar)(({ theme }) => ({
  zIndex: 9,
  width: 64,
  height: 64,
  position: 'absolute',
  left: theme.spacing(19),
  bottom: theme.spacing(-4)
}));

const InfoStyle = styled('div')(({ theme }) => ({
  direction: "row",
  display: 'center',
  justifyContent: 'center',
  alignItems: 'center',
  fontsize: 18,
  marginTop: theme.spacing(2),
  bottom: theme.spacing(-4),
  color: theme.palette.text.disabled
}));

const FollowerStyle = styled('div')(({ theme }) => ({
  direction: "row",
  display: 'flex',
  justifyContent: 'space-around',
  alignItems: 'center',
  marginTop: theme.spacing(3),
  bottom: theme.spacing(-4),
  color: theme.palette.text.disabled
}));

const FollowerItem = styled('div')(({ theme }) => ({
  direction: "column",
  justifyContent: 'center',
  alignItems: 'center',
  // marginTop: theme.spacing(3),
  bottom: theme.spacing(-4),
  color: theme.palette.text.disabled
}));

const CoverImgStyle = styled('img')({
  top: 0,
  width: '100%',
  height: '100%',
  objectFit: 'cover',
  position: 'absolute',
  // opacity: 0.3
});

UserCard.propTypes = {
  user: PropTypes.object.isRequired,
  index: PropTypes.number
};

export default function UserCard({ user, index }) {
  const { avatarUrl, name, company, followerFacebook, followerInstagram, followerTwitter, role, coverUrl } = user;
  const latestPostLarge = false;
  const latestPost = false;

  const ICONS = [
    { icon: facebookFill, color: "rgb(24, 119, 242)" },
    { icon: instagram, color: "rgb(215, 51, 109)" },
    { icon: linkedinFill, color: "rgb(0, 96, 151)" },
    { icon: TwitterFill, color: "rgb(28, 156, 234)" }
  ];

  const FOLLOWER_INFO = [
    followerFacebook , followerInstagram, followerTwitter
  ]

  return (
    <Grid item xs={12} sm={4} md={4}>
      <Card sx={{ position: 'relative' }}>
        <CardMediaStyle
        >
         
          <SvgIconStyle
            color="paper"
            src="/static/icons/shape-avatar.svg" fs
            sx={{
              width: 144,
              height: 62,
              zIndex: 9,
              left: 112,
              bottom: -25.5,
              position: 'absolute',
              ...((latestPostLarge || latestPost) && { display: 'none' })
            }}
          />
          
          <AvatarStyle
            alt={name}
            src={avatarUrl}
          />
          <CoverImgStyle alt={name} src={coverUrl} />
          <FilterStyle/>
        </CardMediaStyle>

        <CardContent
          sx={{
            pl: 0,
            pr: 0,
            pt: 4
          }}
        >

          <TitleStyle
            to="#"
            color="inherit"
            variant="subtitle2"
            component={RouterLink}
          >
            {company}
          </TitleStyle>

          <Typography
            gutterBottom
            variant="caption"
            textAlign="center"
            sx={{ color: 'text.disabled', display: 'block', fontSize: 15, marginTop: -1.5, }}
          >
            {role}
          </Typography>
          <InfoStyle>
            {ICONS.map((info, index) => (
              <Box
                key={index}
                sx={{
                  display: 'flex',
                  alignItems: 'center',
                  ml: index === 0 ? 0 : 1.5
                }}
              >
                <Box color={info.color} component={Icon} icon={info.icon} sx={{ width: 18, height: 18, mr: 0.5 }} />
              </Box>
            ))}
          </InfoStyle>
          <DividerStyle variant="vertical" />
          <FollowerStyle>
            {FOLLOWER_INFO.map((numberFollower, index) => (
              <FollowerItem>
                <Box fontSize={"0.75rem"} key={index}>Follower</Box>
                <Box fontWeight = {600} fontSize={"1rem"} lineHeight={1.5} color = "black">{kFormatter(numberFollower)}</Box>
              </FollowerItem>
            ))}
          </FollowerStyle>
        </CardContent>
      </Card>
    </Grid>
  );
}
