import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
// import { useMemo } from 'react';
// material
import { CssBaseline } from '@material-ui/core';
import { ThemeProvider, createTheme, StyledEngineProvider } from '@material-ui/core/styles';
//
import shape from './shape';
import lightTheme from './lightTheme';
import darkTheme from './darkTheme';
import typography from './typography';
import GlobalStyles from './globalStyles';
import componentsOverride from './overrides';
import { shadows, customShadows } from './shadows';
import SidebarSetting from 'src/layouts/dashboard/SidebarSetting';

ThemeConfig.propTypes = {
  children: PropTypes.node
};

export default function ThemeConfig({ children }) {
  const [themeMode, setThemeMode] = useState('light');
  const existTheme = localStorage.getItem('theme');
  useEffect(() => {
    if (!existTheme) {
      localStorage.setItem('theme', themeMode);
    } else if (existTheme !== themeMode) {
      setThemeMode(existTheme);
    }
  }, [existTheme, themeMode]);
  document.body.classList.add(themeMode ? 'theme-light' : 'theme-dark');
  document.body.classList.remove(themeMode ? 'theme-dark' : 'theme-light');
  const themeOptions = {
    palette: themeMode === 'light' ? lightTheme : darkTheme,
    shape,
    typography,
    shadows: shadows(themeMode === 'light'),
    customShadows: customShadows(themeMode === 'light'),
  };

  const theme = createTheme(themeOptions);
  theme.components = componentsOverride(theme);

  return (
    <StyledEngineProvider injectFirst>
      <SidebarSetting
        theme={theme}
        themeMode={themeMode}
        onHandleChangeTheme={(value) => setThemeMode(value)}
      />
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <GlobalStyles />
        {children}
      </ThemeProvider>
    </StyledEngineProvider>
  );
}
